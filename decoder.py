
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# pylint: disable=no-name-in-module, too-many-arguments, too-many-locals
# pylint: disable=not-context-manager, protected-access, invalid-name

import collections
import copy

import tensorflow as tf
import numpy as np

from tensorflow.contrib.seq2seq import AttentionWrapper
from tensorflow.python.util import nest
from tensorflow.contrib.seq2seq import tile_batch

from texar.modules.decoders.rnn_decoder_base import RNNDecoderBase
from texar.utils import utils

from bleu import compute_bleu


def calc_bleu(refs, hypo, unk_id):
    if len(hypo) == 0 or len(refs[0]) == 0:
        return 0.

    for i in range(len(hypo)):
        assert isinstance(hypo[i], int)
        if hypo[i] == unk_id:
            hypo[i] = -1

    return compute_bleu(reference_corpus=[refs], translation_corpus=[hypo])[0]


class AttentionRNNDecoderOutput(
    collections.namedtuple(
        "AttentionRNNDecoderOutput",
        ["logits", "sample_id", "cell_output",
         "attention_scores", "attention_context"])):
    pass


#TODO(zhiting): allow a list of Attention Mechanisms
class AttentionRNNDecoder(RNNDecoderBase):
    def __init__(self,
                 memory,
                 vocab,
                 ground_truth,
                 ground_truth_length,
                 lambdas,
                 memory_sequence_length=None,
                 cell_input_fn=None,
                 cell=None,
                 cell_dropout_mode=None,
                 vocab_size=None,
                 output_layer=None,
                 hparams=None):
        RNNDecoderBase.__init__(
            self, cell, vocab_size, output_layer, cell_dropout_mode, hparams)

        self._vocab = vocab
        self._ground_truth = ground_truth
        self._lambdas = lambdas
        self._ground_truth_length = ground_truth_length

        attn_hparams = self._hparams['attention']
        attn_kwargs = attn_hparams['kwargs'].todict()

        # Parse the 'probability_fn' argument
        if 'probability_fn' in attn_kwargs:
            prob_fn = attn_kwargs['probability_fn']
            if prob_fn is not None and not callable(prob_fn):
                prob_fn = utils.get_function(
                    prob_fn,
                    ['tensorflow.nn', 'tensorflow.contrib.sparsemax',
                     'tensorflow.contrib.seq2seq'])
            attn_kwargs['probability_fn'] = prob_fn

        attn_kwargs.update({
            "memory_sequence_length": memory_sequence_length,
            "memory": memory})
        self._attn_kwargs = attn_kwargs
        attn_modules = ['tensorflow.contrib.seq2seq', 'texar.custom']
        # Use variable_scope to ensure all trainable variables created in
        # the attention mechanism are collected
        with tf.variable_scope(self.variable_scope):
            attention_mechanism = utils.check_or_get_instance(
                attn_hparams["type"], attn_kwargs, attn_modules,
                classtype=tf.contrib.seq2seq.AttentionMechanism)

        self._attn_cell_kwargs = {
            "attention_layer_size": attn_hparams["attention_layer_size"],
            "alignment_history": attn_hparams["alignment_history"],
            "output_attention": attn_hparams["output_attention"],
        }
        self._cell_input_fn = cell_input_fn
        # Use variable_scope to ensure all trainable variables created in
        # AttentionWrapper are collected
        with tf.variable_scope(self.variable_scope):
            attn_cell = AttentionWrapper(
                self._cell,
                attention_mechanism,
                cell_input_fn=self._cell_input_fn,
                **self._attn_cell_kwargs)
            self._cell = attn_cell

    #TODO(zhiting): fix the TODOs in the docstring
    @staticmethod
    def default_hparams():
        hparams = RNNDecoderBase.default_hparams()
        hparams["name"] = "attention_rnn_decoder"
        hparams["attention"] = {
            "type": "LuongAttention",
            "kwargs": {
                "num_units": 256,
            },
            "attention_layer_size": None,
            "alignment_history": False,
            "output_attention": True,
        }
        return hparams

    def _get_beam_search_cell(self, beam_width):
        """Returns the RNN cell for beam search decoding.
        """
        with tf.variable_scope(self.variable_scope, reuse=True):
            attn_kwargs = copy.copy(self._attn_kwargs)

            memory = attn_kwargs['memory']
            attn_kwargs['memory'] = tile_batch(memory, multiplier=beam_width)

            memory_seq_length = attn_kwargs['memory_sequence_length']
            if memory_seq_length is not None:
                attn_kwargs['memory_sequence_length'] = tile_batch(
                    memory_seq_length, beam_width)

            attn_modules = ['tensorflow.contrib.seq2seq', 'texar.custom']
            bs_attention_mechanism = utils.check_or_get_instance(
                self._hparams.attention.type, attn_kwargs, attn_modules,
                classtype=tf.contrib.seq2seq.AttentionMechanism)

            bs_attn_cell = AttentionWrapper(
                self._cell._cell,
                bs_attention_mechanism,
                cell_input_fn=self._cell_input_fn,
                **self._attn_cell_kwargs)

            self._beam_search_cell = bs_attn_cell

            return bs_attn_cell

    def initialize(self, name=None):
        helper_init = self._helper.initialize()

        flat_initial_state = nest.flatten(self._initial_state)
        dtype = flat_initial_state[0].dtype
        initial_state = self._cell.zero_state(
            batch_size=tf.shape(flat_initial_state[0])[0], dtype=dtype)
        initial_state = initial_state.clone(cell_state=self._initial_state)

        initial_state = [tf.ones((
            tf.shape(flat_initial_state[0])[0], 60),
            dtype=tf.int32) * self._vocab.eos_token_id, initial_state]

        return [helper_init[0], helper_init[1], initial_state]

    def step(self, time, inputs, state, name=None):
        wrapper_outputs, wrapper_state = self._cell(inputs, state[1])
        logits = self._output_layer(wrapper_outputs)

        sample_method_sampler = \
            tf.distributions.Categorical(probs=self._lambdas)
        sample_method_id = sample_method_sampler.sample()

        def _rewards_sample_ids():
            def _get_rewards(time, prefix_ids, target_ids, ground_truth_length):
                batch_size = np.shape(target_ids)[0]
                words_in_target = \
                    [np.unique(target_ids[i]) for i in range(batch_size)]
                unk_id = self._vocab.unk_token_id
                eos_id = self._vocab.eos_token_id

                # before append
                baseline_scores = []
                baseline_ids = prefix_ids[:, :time]
                for i in range(batch_size):
                    ref = target_ids[i].tolist()
                    if self._vocab.eos_token_id in ref:
                        ref = ref[:ref.index(self._vocab.eos_token_id)]

                    hypo = baseline_ids[i].tolist()
                    if self._vocab.eos_token_id in hypo:
                        hypo = hypo[:hypo.index(self._vocab.eos_token_id)]

                    baseline_scores.append(
                        calc_bleu(refs=[ref], hypo=hypo, unk_id=unk_id))

                # append UNK
                syn_ids = np.concatenate([
                    prefix_ids[:, :time],
                    np.ones((batch_size, 1), dtype=np.int32) * unk_id], axis=1)

                reward_unk = []
                for i in range(batch_size):
                    ref = target_ids[i].tolist()
                    if self._vocab.eos_token_id in ref:
                        ref = ref[:ref.index(self._vocab.eos_token_id)]

                    hypo = syn_ids[i].tolist()
                    if self._vocab.eos_token_id in hypo:
                        hypo = hypo[:hypo.index(self._vocab.eos_token_id)]

                    reward_unk.append(
                        np.ones((1, self._vocab_size), dtype=np.float32) *
                        calc_bleu(refs=[ref], hypo=hypo, unk_id=unk_id) -
                        baseline_scores[i])
                result = np.concatenate(reward_unk, axis=0)

                # append tokens
                for i in range(batch_size):
                    for id in words_in_target[i]:
                        syn_id = np.concatenate(
                            [prefix_ids[i:i + 1, :time], np.array([[id, ]])],
                            axis=1)
                        hypo = syn_id[0].tolist()
                        if self._vocab.eos_token_id in hypo:
                            hypo = hypo[:hypo.index(self._vocab.eos_token_id)]

                        ref = target_ids[i].tolist()
                        if self._vocab.eos_token_id in ref:
                            ref = ref[:ref.index(self._vocab.eos_token_id)]

                        dup = 1. if prefix_ids[i][time] == id and \
                                    id != unk_id else 0.
                        eos = 1. if time < ground_truth_length[i] - 1 and \
                                    id == eos_id else 0.

                        result[i][id] = \
                            calc_bleu(refs=[ref], hypo=hypo, unk_id=unk_id) -\
                            baseline_scores[i] - dup - eos

                return result

            sampler = tf.distributions.Categorical(
                logits=tf.py_func(_get_rewards, [
                    time, state[0], self._ground_truth,
                    self._ground_truth_length], tf.float32))
            return tf.reshape(sampler.sample(), (32,))

        truth_feeding = tf.cond(
            tf.less(time, tf.shape(self._ground_truth)[1]),
            lambda : tf.to_int32(self._ground_truth[:, time]),
            lambda : tf.ones_like(self._ground_truth[:, 0],
                                  dtype=tf.int32) * self._vocab.eos_token_id)

        sample_ids = tf.cond(
            tf.equal(sample_method_id, 0),
            lambda : self._helper.sample(
                time=time, outputs=logits, state=wrapper_state),
            lambda : tf.cond(
                tf.equal(sample_method_id, 1),
                lambda : truth_feeding,
                _rewards_sample_ids))

        (finished, next_inputs, next_state) = self._helper.next_inputs(
            time=time,
            outputs=logits,
            state=wrapper_state,
            sample_ids=sample_ids)

        next_state = [tf.concat(
            [state[0][:, :time], tf.expand_dims(sample_ids, 1),
             state[0][:, time + 1:]], axis=1), next_state]
        next_state[0] = tf.reshape(next_state[0], (tf.shape(inputs)[0], 60))

        attention_scores = wrapper_state.alignments
        attention_context = wrapper_state.attention
        outputs = AttentionRNNDecoderOutput(
            logits, sample_ids, wrapper_outputs,
            attention_scores, attention_context)

        return (outputs, next_state, next_inputs, finished)

    def finalize(self, outputs, final_state, sequence_lengths):
        return outputs, final_state

    def _alignments_size(self):
        # Reimplementation of the alignments_size of each of
        # AttentionWrapper.attention_mechanisms. The original implementation
        # of `_BaseAttentionMechanism._alignments_size`:
        #
        #    self._alignments_size = (self._keys.shape[1].value or
        #                       array_ops.shape(self._keys)[1])
        #
        # can be `None` when the seq length of encoder outputs are priori
        # unknown.
        alignments_size = []
        for am in self._cell._attention_mechanisms:
            az = (am._keys.shape[1].value or tf.shape(am._keys)[1:-1])
            alignments_size.append(az)
        return self._cell._item_or_tuple(alignments_size)

    @property
    def output_size(self):
        return AttentionRNNDecoderOutput(
            logits=self._rnn_output_size(),
            sample_id=self._helper.sample_ids_shape,
            cell_output=self._cell.output_size,
            attention_scores=self._alignments_size(),
            attention_context=self._cell.state_size.attention)

    @property
    def output_dtype(self):
        """Types of output of one step.
        """
        # Assume the dtype of the cell is the output_size structure
        # containing the input_state's first component's dtype.
        # Return that structure and the sample_ids_dtype from the helper.
        dtype = nest.flatten(self._initial_state)[0].dtype
        return AttentionRNNDecoderOutput(
            logits=nest.map_structure(lambda _: dtype, self._rnn_output_size()),
            sample_id=self._helper.sample_ids_dtype,
            cell_output=nest.map_structure(
                lambda _: dtype, self._cell.output_size),
            attention_scores=nest.map_structure(
                lambda _: dtype, self._alignments_size()),
            attention_context=nest.map_structure(
                lambda _: dtype, self._cell.state_size.attention))

    def zero_state(self, batch_size, dtype):
        """Returns zero state of the basic cell.

        Same as :attr:`decoder.cell._cell.zero_state`.
        """
        return self._cell._cell.zero_state(batch_size=batch_size, dtype=dtype)

    def wrapper_zero_state(self, batch_size, dtype):
        """Returns zero state of the attention-wrapped cell.

        Same as :attr:`decoder.cell.zero_state`.
        """
        return self._cell.zero_state(batch_size=batch_size, dtype=dtype)

    @property
    def state_size(self):
        """The state size of the basic cell.

        Same as :attr:`decoder.cell._cell.state_size`.
        """
        return self._cell._cell.state_size


    @property
    def wrapper_state_size(self):
        """The state size of the attention-wrapped cell.

        Same as :attr:`decoder.cell.state_size`.
        """
        return self._cell.state_size

