"""Attentional Seq2seq.
"""
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import importlib
import os

import tensorflow as tf
import texar as tx
import numpy as np

from decoder import AttentionRNNDecoder

flags = tf.flags

flags.DEFINE_string("config_model", "config_model", "The model config.")
flags.DEFINE_string("config_data", "config_iwslt14", "The dataset config.")

flags.DEFINE_float('anneal_factor', 0.02, 'anneal factor')

FLAGS = flags.FLAGS

config_model = importlib.import_module(FLAGS.config_model)
config_data = importlib.import_module(FLAGS.config_data)

log_dir = 'training_log_anneal' + '_factor' + str(FLAGS.anneal_factor) + '/'
os.system('mkdir ' + log_dir)


def build_model(data_batch, source_vocab, target_vocab, lambdas):
    """Assembles the seq2seq model.
    """
    batch_size = tf.shape(data_batch['target_length'])[0]
    source_vocab_size = source_vocab.size
    target_vocab_size = target_vocab.size
    target_bos_token_id = target_vocab.bos_token_id
    target_eos_token_id = target_vocab.eos_token_id

    source_embedder = tx.modules.WordEmbedder(
        vocab_size=source_vocab_size, hparams=config_model.embedder)

    encoder = tx.modules.BidirectionalRNNEncoder(
        hparams=config_model.encoder)

    enc_outputs, _ = encoder(source_embedder(data_batch['source_text_ids']))

    embedder = tx.modules.WordEmbedder(
        vocab_size=target_vocab_size, hparams=config_model.embedder)

    decoder = AttentionRNNDecoder(
        memory=tf.concat(enc_outputs, axis=2),
        memory_sequence_length=data_batch['source_length'],
        vocab=target_vocab,
        ground_truth=data_batch['target_text_ids'][:, 1:],
        ground_truth_length=data_batch['target_length'] - 1,
        vocab_size=target_vocab_size,
        lambdas=lambdas,
        hparams=config_model.decoder)

    start_tokens = \
        tf.ones_like(data_batch['target_length']) * target_bos_token_id
    training_outputs, _, training_length = decoder(
        decoding_strategy='infer_sample',
        initial_state=decoder.zero_state(
            batch_size=batch_size, dtype=tf.float32),
        max_decoding_length=60,
        embedding=embedder,
        start_tokens=start_tokens,
        end_token=target_eos_token_id,
        sequence_length=data_batch['target_length'] - 1)

    train_op = tx.core.get_train_op(
        tx.losses.sequence_sparse_softmax_cross_entropy(
            labels=training_outputs.sample_id,
            logits=training_outputs.logits,
            sequence_length=training_length))

    start_tokens = \
        tf.ones_like(data_batch['target_length']) * target_bos_token_id
    beam_search_outputs, _, _ = \
        tx.modules.beam_search_decode(
            decoder_or_cell=decoder,
            embedding=embedder,
            start_tokens=start_tokens,
            end_token=target_eos_token_id,
            beam_width=config_model.beam_width,
            max_decoding_length=60)

    return train_op, training_outputs, beam_search_outputs


def main():
    """Entrypoint.
    """
    training_data = tx.data.PairedTextData(hparams=config_data.train)
    valid_data = tx.data.PairedTextData(hparams=config_data.valid)
    test_data = tx.data.PairedTextData(hparams=config_data.test)
    data_iterator = tx.data.TrainTestDataIterator(
        train=training_data, val=valid_data, test=test_data)

    source_vocab = training_data.source_vocab
    target_vocab = training_data.target_vocab

    data_batch = data_iterator.get_next()
    lambdas_ts = tf.placeholder(shape=[3,], dtype=tf.float32)

    train_op, training_outputs, infer_outputs = build_model(
        data_batch, source_vocab, target_vocab, lambdas_ts)

    def _train_epoch(sess, epoch, lambdas):
        data_iterator.switch_to_train_data(sess)
        log_file = open(log_dir + 'training_log' + str(epoch) + '.txt', 'w')

        step = 0
        while True:
            try:
                assert min(lambdas) > -1e-5
                loss = sess.run(train_op, feed_dict={
                    lambdas_ts: np.array(lambdas)})
                print("step={}, loss={:.4f}, lambdas={}".format(
                    step, loss, lambdas), file=log_file)
                log_file.flush()
                step += 1

            except tf.errors.OutOfRangeError:
                break

    def _eval_epoch(sess, mode):
        if mode == 'valid':
            data_iterator.switch_to_val_data(sess)
        else:
            data_iterator.switch_to_test_data(sess)

        refs, hypos = [], []
        while True:
            try:
                fetches = [
                    data_batch['target_text'][:, 1:],
                    infer_outputs.predicted_ids[:, :, 0]
                ]
                feed_dict = {
                    tx.global_mode(): tf.estimator.ModeKeys.PREDICT
                }
                target_texts_ori, output_ids = \
                    sess.run(fetches, feed_dict=feed_dict)

                target_texts = \
                    [s[:s.index('<EOS>')]for s in target_texts_ori.tolist()]
                output_texts = tx.utils.map_ids_to_strs(
                    ids=output_ids, vocab=valid_data.target_vocab)

                for hypo, ref in zip(output_texts, target_texts):
                    hypos.append(hypo)
                    refs.append([ref])
            except tf.errors.OutOfRangeError:
                break

        return tx.evals.corpus_bleu_moses(
            list_of_references=refs, hypotheses=hypos)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        sess.run(tf.tables_initializer())

        lambdas = [0.01, 0.99, 0.]

        scores_file = open(log_dir + 'scores.txt', 'w')
        best_valid_bleu = -1.
        best_tmp = -1.
        for i in range(config_data.num_epochs):
            _train_epoch(sess, i, lambdas)

            valid_bleu = _eval_epoch(sess, 'valid')
            test_bleu = _eval_epoch(sess, 'test')

            if valid_bleu < best_tmp and lambdas[2] < 0.9:
                lambdas[1] -= FLAGS.anneal_factor
                lambdas[2] += FLAGS.anneal_factor
                best_tmp = -1.
            best_tmp = max(valid_bleu, valid_bleu)

            best_valid_bleu = max(best_valid_bleu, valid_bleu)
            print('valid epoch={}, BLEU={}; best-ever={}'.format(
                i, valid_bleu, best_valid_bleu), file=scores_file)
            print('test epoch={}, BLEU={}'.format(
                i, test_bleu),file=scores_file)
            print('=' * 50, file=scores_file)
            scores_file.flush()


if __name__ == '__main__':
    main()
